﻿using System;

namespace ArithmeticSequenceTask
{
    public static class ArithmeticSequence
    {
        /// <summary>
        /// Calculates the sum of the first 'count' elements of a sequence in which each element is the sum of the given integer 'number'
        /// and number of occurrences of the given integer 'add', based on the element's position within the sequence.
        /// </summary>
        /// <param name="number">Source number.</param>
        /// <param name="add">The term.</param>
        /// <param name="count">The number of occurrences.</param>
        /// <returns>Calculated sum.</returns>
        /// <exception cref="OverflowException">
        /// Thrown when number is int.MaxValue and term more than 0
        /// - or -
        /// number is int.MinValue and term less than 0.
        /// </exception>
        /// <exception cref="ArgumentException">Thrown if count is less than zero.</exception>
        public static int Calculate(int number, int add, int count)
        {
            if (count < 0)
            {
                throw new ArgumentException("Count cannot be less than zero.");
            }

            if (number == int.MaxValue && add > 0)
            {
                throw new OverflowException($"The sum of number ({number}) and term ({add}) exceeds the maximum value of an integer.");
            }

            if (number == int.MinValue && add < 0)
            {
                throw new OverflowException($"The sum of number ({number}) and term ({add}) exceeds the minimum value of an integer.");
            }

            int sum = 0;

            for (int i = 0; i < count; i++)
            {
                try
                {
                    sum = checked(sum + number);
                }
                catch (OverflowException ex)
                {
                    throw new OverflowException("The sum of the sequence exceeds the maximum value of an integer.", ex);
                }

                number += add;
            }

            return sum;
        }
    }
}
